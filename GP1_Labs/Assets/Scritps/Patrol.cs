using System.Collections;
using System.Collections.Generic;
using UnityEditor.Rendering;
using UnityEngine;

public class Patrol : MonoBehaviour
{
    [SerializeField]private float speed = 5f; // Movement speed, exposed in the editor

    [SerializeField]private List<Vector3> points = new List<Vector3>(); // Points between which the sprite will move

    private int targetPointIndex = 0; // Index of the current target point in the list

    void Start()
    {
        
    }


    void Update()
    {
        if (points.Count < 2) return; // Ensure there are at least two points

        MoveTowardsTarget();


    }
    private void MoveTowardsTarget()
    {
        // Move towards the current target point
        transform.position = Vector3.MoveTowards(transform.position, points[targetPointIndex], speed * Time.deltaTime);

        // Check if the sprite has reached the target point
        if (Vector3.Distance(transform.position, points[targetPointIndex]) < 0.001f)
        {
            // Switch target point: If at the first point, switch to the second; if at the second, switch back to the first
            targetPointIndex = (targetPointIndex + 1) % points.Count;
        }
    }
}
